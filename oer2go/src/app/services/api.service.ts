import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {concatMap, map} from "rxjs/operators";
import {forEach} from "../../../node_modules/@angular/router/src/utils/collection";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API_BASE = 'http://oer2go.org/cgi/json_api_v2.pl';

  // http://oer2go.org/cgi/json_api_v2.pl?moddir=en-wikipedia
  // http://oer2go.org/cgi/json_api_v2.pl

  constructor(private http: HttpClient) { }

  getCategories(): Observable<any> {
    return this.http.get(this.API_BASE);

  }
  getCategory(filter: string ): Observable<any> {
    return this.http.get(`${this.API_BASE}?moddir=${filter}`);
  }
}
