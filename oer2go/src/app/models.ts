/**
 * Data from API as List entries
 */
export interface IStuff {
  lang: string
  title: string
  is_hidden: boolean
  moddir: string
  logofilename: string
  version: string
  k_size: number
  file_count: number
  moduleId: number

}

/**
 * Detailed Information for a category
 */
export interface IStuffDetail extends IStuff {
  // lang: string,
  rating: number
  // title: string,
  prereq_note: string
  prereq_id: number
  // version: string
  cc_license: string
  module_id: number
  // kSize: number
  type: string
  kiwix_url: string
  logofilename: string
  // is_hidden: boolean
  kiwix_date: string
  // moddir: string
  // file_count: number
  ageRange: string
  source_url: string
  category: string
  description: string
}
