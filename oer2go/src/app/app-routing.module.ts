import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {InfoComponent} from "./components/info/info.component";

const routes: Routes = [
  // app routes
  { path: 'home', component: HomeComponent },
  { path: 'info', component: InfoComponent},

  // tooling routes
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
