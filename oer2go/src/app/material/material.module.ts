import {NgModule} from '@angular/core';
import {
  MatCardModule,
  MatExpansionModule,
  MatGridListModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCheckboxModule,
  MatIconModule,
  MatInputModule,
  MatToolbarModule,
  MatTableModule
} from '@angular/material';

@NgModule({
  imports: [
    MatCardModule,
    MatExpansionModule,
    MatGridListModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    MatTableModule
  ],
  exports: [
    MatCardModule,
    MatExpansionModule,
    MatGridListModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    MatTableModule
  ],
  declarations: []
})
export class MaterialModule {
}
