import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {concatMap, flatMap, map, toArray, reduce, filter} from "rxjs/operators";
import {IStuff, IStuffDetail} from '../../models';

  @Component({
    selector: 'oer-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.sass']
  })
  export class HomeComponent implements OnInit {

    step = 0;
    categories: IStuff[] = [];
    activeCategory: any = {};

    constructor(private api: ApiService) {
    }

    ngOnInit() {
      console.log('init');

      this.api.getCategories()
        .pipe(
          // ignore keys, we just need those values
          map((item) => Object.values(item))
        )
        .subscribe((data) => {
          console.log('data', data);
          this.categories = (data as IStuff[]);
        });
    }

    setStep(index: number) {
      this.step = index;
      this.api.getCategory(this.categories[index].moddir)
        .subscribe((data) => {
          console.log('detail data', data);
          this.activeCategory = (data as IStuffDetail);
        })
    }

    nextStep() {
      this.step++;
    }

    prevStep() {
      this.step--;
    }
  }
