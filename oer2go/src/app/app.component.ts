import { Component } from '@angular/core';

enum IconExpanded {
  EXPANDED = 'expand_less',
  NOT_EXPANDED = 'expand_more'
}

@Component({
  selector: 'oer-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  state = {
    searchExpanded: false
  };

  toggleSearchExpanded() {
    this.state.searchExpanded = !this.state.searchExpanded;
  }

  searchIconForState() : string {
    return this.state.searchExpanded ? IconExpanded.EXPANDED : IconExpanded.NOT_EXPANDED;
  }
}
