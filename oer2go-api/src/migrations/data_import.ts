import {from, fromEvent, of} from "rxjs";
import {map} from "rxjs/operators";
import * as sqlite from "sqlite";
import Promise from 'bluebird';
import axios from 'axios';
// @ts-ignore
import * as values from 'object.values';

values.shim();

const API_BASE = 'http://oer2go.org/cgi/json_api_v2.pl';

from(axios.get(API_BASE))
  .pipe(
    map((response) => Object.values(response.data))
  )
  .subscribe((data) => {

    from(sqlite.open('./oer2go.db'))
      .subscribe((con) => {

        data.forEach((item: any) => {
          console.log('---> ', item.lang);

          con.exec(
            `INSERT INTO categories 
                        (lang, rating, title, prereq_note, prereq_id, version, cc_license, module_id, ksize, type, kiwix_url, logofilename, is_hidden, kiwix_date, moddir, file_count, age_range, source_url, category, description)
                VALUES ( ${item.lang}, ${item.rating}, ${item.title}, ${item.prereq_note}, ${item.prereq_id}, ${item.version}, ${item.cc_license}, ${item.module_id}, ${item.ksize}, ${item.type}, ${item.kiwix_url}, ${item.logofilename}, ${item.is_hidden}, ${item.kiwix_data}, ${item.moddir}, ${item.file_count}, ${item.max_range}, ${item.source_url}, ${item.category}, ${item.description});`
          ).then( (d) => {
            console.log('exec', d);
            con.close();
          });
        });
      });
  });
