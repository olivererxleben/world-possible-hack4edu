"use strict";
exports.__esModule = true;
var express = require("express");
var sqlite = require("sqlite");
var rxjs_1 = require("rxjs");
var db = rxjs_1.from(sqlite.open('./oer2go.db'));
var App = (function () {
    function App() {
        this.express = express();
        this.mountRoutes();
    }
    App.prototype.mountRoutes = function () {
        var router = express.Router();
        router.get('/', function (req, res) {
            res.json({
                message: 'Hello World!!!'
            });
        });
        this.express.use('/', router);
    };
    return App;
}());
exports["default"] = new App().express;
//# sourceMappingURL=app.js.map