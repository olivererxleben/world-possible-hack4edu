"use strict";
exports.__esModule = true;
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var sqlite = require("sqlite");
var axios_1 = require("axios");
var values = require("object.values");
values.shim();
var API_BASE = 'http://oer2go.org/cgi/json_api_v2.pl';
rxjs_1.from(axios_1["default"].get(API_BASE))
    .pipe(operators_1.map(function (response) { return Object.values(response.data); }))
    .subscribe(function (data) {
    rxjs_1.from(sqlite.open('./oer2go.db'))
        .subscribe(function (con) {
        data.forEach(function (item) {
            console.log('---> ', item.lang);
            con.exec("INSERT INTO categories \n                        (lang, rating, title, prereq_note, prereq_id, version, cc_license, module_id, ksize, type, kiwix_url, logofilename, is_hidden, kiwix_date, moddir, file_count, age_range, source_url, category, description)\n                VALUES ( " + item.lang + ", " + item.rating + ", " + item.title + ", " + item.prereq_note + ", " + item.prereq_id + ", " + item.version + ", " + item.cc_license + ", " + item.module_id + ", " + item.ksize + ", " + item.type + ", " + item.kiwix_url + ", " + item.logofilename + ", " + item.is_hidden + ", " + item.kiwix_data + ", " + item.moddir + ", " + item.file_count + ", " + item.max_range + ", " + item.source_url + ", " + item.category + ", " + item.description + ");").then(function (d) {
                console.log('exec', d);
                con.close();
            });
        });
    });
});
//# sourceMappingURL=data_import.js.map